// Create package main
// Add all required imports
// Create a function that greets the CI/CD component user
// Add functions to calculate PI
// Add function that allocates 1 MB memory
// Call all functions in the main package, and main function
// End with saying that everything was generated with the help of GitLab Duo

package main

import (
  "fmt"
  "math"
)

func greetUser(name string) {
  fmt.Printf("Hello %s, welcome to the CI/CD component!\n", name)
}

func calculatePi(iterations int) float64 {
  var pi float64 = 0
  for i := 0; i < iterations; i++ {
    pi += math.Pow(-1, float64(i)) * 4 / (2*float64(i) + 1)
  }
  return pi
}

func allocateMemory() {
  data := make([]byte, 1024*1024)
  fmt.Printf("Allocated %d MB of memory\n", len(data)/1024/1024)
}

func main() {
  greetUser("GitLab Duo")
  fmt.Printf("Value of Pi calculated to 10 iterations: %.10f\n", calculatePi(10))
  allocateMemory()
  fmt.Println("Everything was generated with the help of GitLab Duo!")
}
