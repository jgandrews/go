# Go CI/CD Component

> 🚧 **NOTE** 🚧
>
> This component is [being migrated from CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/issues/437104) and **work-in-progress**, where inputs, template names, and interfaces might change through 0.x.y releases.
>
> Please wait for 1.x.y releases for production usage.

This CI/CD component provides job templates to format, build and test Go source code.

## Build

The `build` component runs `go build`.

```yaml
include:
  - component: gitlab.com/components/go/build@<VERSION>
    inputs:
      job_name: build
      go_image: golang:latest
      stage: build
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `build` | The job name |
| `stage` | `build` | The stage name |
| `go_image` | `golang:latest` | The Go image for CI/CD job |

## Test

The `test` component runs `go test`.

```yaml
include:
  - component: gitlab.com/components/go/test@<VERSION>
    inputs:
      job_name: test
      go_image: go:latest
      stage: test
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `test` | The job name |
| `stage` | `test` | The stage name |
| `go_image` | `golang:latest` | The Go image for CI/CD job |

## Format

The `format` component runs `go fmt`.

```yaml
include:
  - component: gitlab.com/components/go/format@<VERSION>
    inputs:
      job_name: format
      go_image: golang:latest
      stage: format
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `job_name` | `format` | The job name |
| `stage` | `format` | The stage name |
| `go_image` | `golang:latest` | The Go image for CI/CD job |

## Full pipeline

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: gitlab.com/components/go/full-pipeline@<VERSION>
```

where `<VERSION>` is the latest released tag, `main` or `~latest`.

The full pipeline requires the following stages defined:

```yaml
stages:
  # Component specific stages
  - format
  - build
  - test
```

### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `go_image` | `golang:latest` | The Go image for CI/CD job |

## Contributing

<!-- Notes and how to get intouch with the maintainers. Usually the contribution process should follow the official guide in https://docs.gitlab.com/ee/ci/components/index.html -->

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 

This component includes a working Go project, with the source code generated with the help of AI-powered [GitLab Duo](https://about.gitlab.com/gitlab-duo/). You can inspect the source code in [main.go](main.go). 

The [.gitlab-ci.yml](.gitlab-ci.yml) configuration tests all components against different Go images, and more inputs.

Author: Michael Friedrich, @dnsmichi